//Function Declaration
function getAPIName(number){
  switch(number){
    case 1:
      document.getElementById("APIName1").innerHTML = "<input id='APIName1_btn' class='NoBackgroundButton' type='button' value='Microsofts Face API' onclick='updateBody(1)'>";
      break;
    case 2:
      document.getElementById("APIName2").innerHTML =  "<input id='APIName2_btn' class='NoBackgroundButton' type='button' value='Amazons Rekognition' onclick='updateBody(2)'>";
      break;
    case 3:
      document.getElementById("APIName3").innerHTML = "<input id='APIName3_btn' class='NoBackgroundButton' type='button' value='Kairos' onclick='updateBody(3)'>";
      break;
    case 4:
      document.getElementById("APIName4").innerHTML =  "<input id='APIName4_btn' class='NoBackgroundButton' type='button' value='Facial Recognition and Detection' onclick='updateBody(4)'>";
      break;
    case 5:
      document.getElementById("APIName5").innerHTML =  "<input id='APIName2_btn' class='NoBackgroundButton' type='button' value='Face++' onclick='updateBody(5)'>";
      break;
    default:
      console.log("Uh oh. There was an issue in the getAPIName function.");
      return "";
      //Just returning nothing here because hopefully this clause never has to trigger.
  }
}

function updateBody(input){
  switch(input){
    case 1:
      document.getElementById("APIDesc").innerHTML = "<h3>Currently Selected API: Microsoft's Face API</h3>";
      break;
    case 2:
      document.getElementById("APIDesc").innerHTML = "<h3>Currently Selected API: Amazon's Rekognition</h3>";
      break;
    case 3:
      document.getElementById("APIDesc").innerHTML = "<h3>Currently Selected API: Kairos</h3>";
      break;
    case 4:
      document.getElementById("APIDesc").innerHTML = "<h3>Currently Selected API: Facial Recogntion and Detection</h3>";
      break;
    case 5:
      document.getElementById("APIDesc").innerHTML = "<h3>Currently Selected API: Face++</h3>";
    default:
      //Do nothing
      return "";
  }

  document.getElementById("tableBody").innerHTML = bodyText;
}

function demoWarning(){
  document.getElementById("jsonArea").innerHTML = "";
  window.alert("In the final version, this will actually do something productive.");
}

function readTextFile(file)
{
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                alert(allText);
            }
        }
    }
    rawFile.send(null);
}

var readTextFileResponse;

function readTextFile(file) {
    /*
      So this code was taken from here: https://stackoverflow.com/questions/14446447/how-to-read-a-local-text-file
      This function is just to read json.txt and throw it into a section on the page.
      In the actual product the image will be processed and the API will most likely return a JSON response, which
      will go in instead. This is just to give an impression of what the API response would look like to an end user.
    */

    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                readTextFileResponse = rawFile.responseText;
                return readTextFileResponse;
            }
        }
    }
    rawFile.send(null);
}

function genJSON(){
  readTextFile("json.txt");
  var jsonReturn = readTextFileResponse;
  var obj = JSON.parse(jsonReturn);
  document.getElementById("jsonArea").innerHTML = obj[0].name + ", " + obj[0].email+ "<br> " + obj[0].address;
}

var bodyText = `
<table style='table-layout: fixed; width: 100%;'>
<tr style='border: 1px solid black;'>
  <th colspan='5' style='border: 1px solid black;'>
  <br><br>
  This will be where the bank of images goes.
  <br><br><br>
  </th>
</tr>
<br>
<tr>
  <td style='border: 1px solid black;'>
    <center>Input Image Section<br>

    <form action="upload.php" method="post" enctype="multipart/form-data">
      Select image to upload:
      <input type="file" name="fileToUpload" id="fileToUpload"><br>
      <input type="submit" value="Upload Image" name="submit">
    </form>
    </center>
  </td>
  <td style='border: 1px solid black;'>
    <center>
    <input type='submit' value='Submit Image' name='submitImage' onclick='genJSON()'><br>
    <input type='submit' value='Cancel' name='cancelImage' onclick='demoWarning()'>
    </center>
  </td>
  <td id='jsonArea' colspan="3" style='border: 1px solid black; text-align: center;'>
      This section will have the output.
  </td>
</tr>
</table>
`
