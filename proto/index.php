<?php
  session_start();
?>
<html>
  <head>
      <title>IFB398 Prototype Mockup</title>
      <script src='script.js'></script>
      <style>
        .NoBackgroundButton {
          width: 100%;
          background: none;
          border: 1px solid black;
        }
      </style>
  </head>

  <body>
    <?php
    //This brings across and shows any messages as a result of uploading an image.
      if(isset($_SESSION['output'])){
        $message = $_SESSION['output'];
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    ?>
    <center><h1>IFB398 Mockup</h1>
    </center>

    <table style='border: 1px solid black; table-layout: fixed; width: 100%;'>
      <thead>
        <tr>
          <td colspan="5" style='text-align: center;'>
            <h2>API Selection</h2>
          </td>
        </tr>
        <tr>
          <th id='APIName1'>
            <script>getAPIName(1);</script>
          </th>
          <th id='APIName2'>
              <script>getAPIName(2);</script>
          </th>
          <th id='APIName3'>
              <script>getAPIName(3);</script>
          </th>
          <th id='APIName4'>
              <script>getAPIName(4);</script>
          </th>
          <th id='APIName5'>
              <script>getAPIName(5);</script>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="5" id='APIDesc' style='text-align: center; vertical-align: middle;'></td>
        </tr>
        <tr>
          <td colspan='5' id='tableBody'></td>
        </tr>
      </tbody>
    </table>
  </body>

</html>
