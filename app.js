var express = require('express');
var app = express();
var http = require('http').Server(app);
var request = require('request');
var rp = require('request-promise');
var formidable = require('formidable'); // File uploading handling
var fs = require('fs'); // file stuff
var mysql = require('mysql');
const path	= require("path");
const dateTime = require('date-time');
const baseImageDir = __dirname + "/client/data/images/";
const publicIp = require('public-ip');


// This will break if you don't have aws setup - don't merge this branch
var AWS = require('aws-sdk');

var address;

// External Files handling API-related stuff
var faceAPI = require('./Face++');
var microAPI = require('./Microsoft');
var skyAPI = require('./SkyBiometry');
var AmazonAPI = require('./Amazon')


// MySQL Connetion Details- Probably best to have this hidden in an external file
var con = mysql.createConnection({
    host: "35.189.8.139",
    user: "jayden",
    password: "password",
    database: "images"
});

// MySQL Connection
con.connect(function(err){
  if(err) throw err;
  console.log("Connected to Database");
})


// Tell Express Site's root is /client/ folder
app.use(express.static('client'));



// Routing
app.get("/", function(error, response, body){
  response.sendFile(path.join(__dirname+'/client/index.html'));
})
app.get("/Kairos", function(error, response, body){
  response.sendFile(path.join(__dirname+'/client/Kairos.html'));
})
app.get("/Microsoft", function(error, response, body){
  response.sendFile(path.join(__dirname+'/client/Microsoft.html'));
})
app.get("/Amazon", function(error, response, body){
  response.sendFile(path.join(__dirname+'/client/Amazon.html'));
})
app.get("/SkyBiometry", function(error, response, body){
  response.sendFile(path.join(__dirname+'/client/SkyBiometry.html'));
})
app.get("/Face", function(error, response, body){
  response.sendFile(path.join(__dirname+'/client/Face++.html'));
})
app.get("/Results", function(error, response, body){
  response.sendFile(path.join(__dirname+'/client/results.html'));
});
app.get("/Test", function(error, response, body){
  response.sendFile(path.join(__dirname+'/client/test.html'));
})
app.get("/Admin", function(error, response, body){
  response.sendFile(path.join(__dirname+'/client/admin.html'));
})
app.get("/Images", function(error, response, body){
  response.sendFile(path.join(__dirname+'/client/images.html'));
})

// Posting
app.post("/fileupload", function(req, res){
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {


      var fileType = files.filetoupload.name.split('.').pop();
      var directories = files.filetoupload.name.split('.');
      var fileName = directories[0];
      var userDirectory = directories[directories.length - 2];
      //console.log(fileName);
      //console.log('______________________________________');
      var userID = fields.user;
      //console.log(userID);
      var fileType = files.filetoupload.name.split('.').pop();
      if(fileType == 'jpg' || fileType == 'png' || fileType == 'jpeg'){
        con.query("SELECT * FROM images ORDER BY ImageID DESC LIMIT 1", function (err, result, fields) {
          if (err) throw err;
            var ID;
          if(result[0] != undefined){
             ID = result[0].ID + 1;
          }else{
            ID = 0;
          }
          var date = dateTime();


          con.query("INSERT INTO images (Extension, UploadDate, IP, fileName, personID) VALUES (?,?,?,?,?)",[fileType,date,"test",fileName,userID], function(err){
                //INSERT INTO images (Extension, UploadDate, IP, fileName, personID) VALUES (?,?,?,?,?)",[fileType,date,"test",fileName,id], function(err)
            if(err)
            throw err;
          });


          var oldpath = files.filetoupload.path;

          con.query("SELECT NAME FROM people WHERE personID = (?)",userID, function(err, result, fields){

            //console.log(result);
            var personName = result[0].NAME + "/";
            //console.log(personName);

            var newpath = baseImageDir + personName + fileName + "." + fileType;
            //console.log("Base: " + baseImageDir + " name: " + personName + " fileName: " + "." + fileName);

            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;

                //console.log("File uploaded to " + newpath);
                res.write('1');
                res.end();
              });
            });
            })
        }else{
          console.log("File uploaded - not an Image");
        }

 });

})
app.post("/uploadSet", function(req, res){
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {



      var fileType = files.filetoupload.name.split('.').pop();
      var directories = files.filetoupload.name.split('/');
      var fileName = directories[directories.length - 1];
      var userDirectory = directories[directories.length - 2];





      if(fileType == 'jpg' || fileType == 'png' || fileType == 'jpeg'){

          var date = dateTime();
          var id;

          if (!fs.existsSync(baseImageDir + userDirectory)){
            con.query("INSERT INTO people (Name) VALUES (?)",[userDirectory], function(err){
              if(err)
              throw err;
            });
              fs.mkdirSync(baseImageDir + userDirectory);
          }

          //console.log(test);
          //console.log(directories[0]);
          var checkAgain = true;
          while(checkAgain){
            checkAgain = false;
            //console.log(directories[1]);

            con.query("SELECT PersonID FROM people where Name = (?)",directories[1], function(err, result, fields){

              if(err){
                throw err;
                checkAgain = true;
              }
              //console.log(result);

                if(result[0] == undefined){
                  checkAgain = true;
                }

                if(!checkAgain){
                  id = result[0].PersonID;
                //id = result
                //console.log("inserting image");

                con.query("INSERT INTO images (Extension, UploadDate, IP, fileName, personID) VALUES (?,?,?,?,?)",[fileType,date,"test",fileName,id], function(err){
                  if(err)
                  throw err;
                });
              }
            })
          }

          var oldpath = files.filetoupload.path;
          var newpath = baseImageDir + userDirectory +"/"+ fileName;



          fs.rename(oldpath, newpath, function (err) {
              if (err){
                console.log("Error uploading image");
                res.write('0');
                res.end();
              } else {
                console.log("File uploaded to " + newpath);
                res.write('1');
                res.end();
              }




            });



      }else{
        console.log("File uploaded - not an Image");
      }
 });

})


app.get("/retrieveMicroImages/:personID/:api", function(req, res){
  var personID = req.params.personID;
  var api = req.params.api;
  con.query("SELECT * FROM images WHERE personID = (?)",personID, function(err, results, fields){
    var images = [];

    var names = [];
    var existsPromises = []

    for (var i = 0; i < results.length; i++) {
      //var object = {ImageID: results[i].ImageID, fileName: results[i].fileName};
      images.push(results[i].ImageID);
      names.push(results[i].fileName);
      existsPromises.push(determineMicrosoftHasImage(results[i].ImageID));
    }
    var exists = [];
    Promise.all(existsPromises).then(function(Exists){
      for (var i = 0; i < results.length; i++) {

        exists.push(Exists[i]);
      }



    var locationPromises = [];
    var count = 0;
    for (var i = 0; i < images.length; i++) {
        locationPromises.push(determineImageLocation(images[i]));

      }

      Promise.all(locationPromises).then(function(results){
        var locations = results;
        var response = {
          images: locations,
          iDs: images,
          names: names,
          exists: exists,
        }
        res.send(response);
      })
    })

    //res.send(locations);
  })
})


// Add person to an API's face list
app.get("/addPerson/:api/:personID", function(request, response, body){
  var api = request.params.api;
  var personID = request.params.personID;
  con.query("SELECT NAME FROM people WHERE PersonID = (?)",personID, function(err, result, fields){
    var name = result[0].NAME;
    response.send("0");
    if(api = 'Microsoft'){
      microAPI.addPerson(name).then(function(result){
        console.log("Method: "+result);
        if(result != undefined){
        con.query("INSERT INTO microsoftpeople (uniqueID, personID) VALUES (?,?)",[result,personID], function(err){
          console.log("Person added to database successfully");
        });
      }else{
        console.log("Person not added to database");
      }
      });
    }
  })

})

app.get("/addFace/:api/:personID/:imageID", function(request, response, body){
  console.log("Adding Face");
  var api = request.params.api;
  var personID = request.params.personID;
  var imageID = request.params.imageID;

  if(api == 'Microsoft'){
    con.query("SELECT * FROM microsoftpeople WHERE personID = (?)",personID, function(err, result, fields){
      if(result[0] == null){
        console.log("Microsoft does not have a person stored, creating")
        con.query("SELECT NAME FROM people WHERE PersonID = (?)",personID, function(err, result, fields){
          var name = result[0].NAME;
          microAPI.addPerson(name).then(function(result){

            if(result != undefined){
            con.query("INSERT INTO microsoftpeople (uniqueID, personID) VALUES (?,?)",[result,personID], function(err){
              console.log("Person added to database successfully");
              con.query("SELECT * FROM microsoftpeople WHERE personID = (?)",personID, function(err, result, fields){
              var uniqueID = result[0].uniqueID;
              determineImageLocation(imageID).then(function(location){
                console.log("Adding Face");
                microAPI.addFace(uniqueID, location).then(function(results){

                  var persistedFaceId = results;
                  var date = dateTime();
                  con.query("INSERT INTO microsoftset (uniqueID, imageID, dateTime) VALUES (?,?,?)",[persistedFaceId,imageID,date], function(err){
                    if(err)
                    throw err;
                      console.log("Face Adding Success!");
                      response.send("Success");
                    })
                  })
                  });
                })

            });
          }else{
            console.log("Person not added to database");
          }
        })
      })
    }else{
      var uniqueID = result[0].uniqueID;
      determineImageLocation(imageID).then(function(location){
        console.log("Adding Face");
        microAPI.addFace(uniqueID, location).then(function(results){

          var persistedFaceId = results;
          var date = dateTime();
          con.query("INSERT INTO microsoftset (uniqueID, imageID, dateTime) VALUES (?,?,?)",[persistedFaceId,imageID,date], function(err){
            if(err)
            throw err;
              console.log("Face Adding Success!");
              response.send("Success");

          });
        })
      })
      }
    })
  }
  //response.send('0');
})

app.get("/delFace/:api/:personID/:imageID", function(request, response, body){
  var api = request.params.api;
  var personID = request.params.personID;
  var imageID = request.params.imageID;

  if(api == 'Microsoft'){
    con.query("SELECT * FROM microsoftset WHERE imageID = (?)", imageID, function(err, result, fields){
      var persistID = result[0].uniqueID;

    con.query("SELECT * FROM microsoftpeople WHERE personID = (?)",personID, function(err, result, fields){
      var uniqueID = result[0].uniqueID;


        microAPI.delFace(uniqueID, persistID).then(function(results){
          console.log("success probably: " + results);
          con.query("DELETE FROM microsoftset WHERE uniqueID = (?)",persistID, function(err){
            if(err)
            throw err;
              console.log("IT APPARENTLY WORKED");
              response.send("Success");

          });

        })


    })
      })
  }
  //response.send('0');

})

  // Retrieve a list of all of the people's names from database
app.get("/retrieveNames", function(req, res){
  con.query("SELECT * FROM people", function(err, result, fields){

    if(err)throw err;

    //console.log(result);

    res.send(result);

  });

})

  // Retrieve a list of images associated to a person
app.get("/retrieveImages/:personID", function(request, response, body){
  var personID = request.params.personID;

  con.query("SELECT * FROM images WHERE personID = (?)",personID, function(err, results, fields){
    var images = [];
      for (var i = 0; i < results.length; i++) {
        var object = {ImageID: results[i].ImageID, fileName: results[i].fileName};
        images.push(object);
      }
      response.send(images);
    })



})

app.get("/retrieveImageLoc/:imageID", function(request, response, body){
  var imageID = request.params.imageID;
  determineImageLocation(imageID).then(function(result){
    console.log("sending: "+result);
    response.send(result);
  })
})
  // Facial Recognition API Testing
app.get("/testAPI/:Method/:API/:Person/:Image", function(request, response, body){
  // Grab the parameters given by the user, assign them to variables
  var method = request.params.Method;
  var api = request.params.API;
  var person = request.params.Person;
  var imageID = request.params.Image;
  determineImageLocation(imageID).then(function(location){

    console.log("Image ID: " + imageID + "Image Location: " + location);
    // Determine which api is being used, call appropriate function
    switch (api) {

      // Kairos API
      case 'Kairos':
        // Sprint 3
      break;


      // Microsoft API
      case 'Microsoft':

      testMicro(method, api, person, imageID, location).then(function(result){
          console.log(result);
          response.send(result);
      })
      break;


      // Amazon API
      case 'Amazon':
        // Sprint 3
        testAmazon(method, api, person, imageID, location).then(function(result){
          console.log(result);
          response.send(result);
        })
      break;


      // SkyBiometry API
      case 'SkyBiometry':

      testSky(method, api, person, imageID, location).then(function(result){
          console.log(result);
          response.send(result);
      })
      break;


      // Face++ API
      case 'Face':

      testFace(method, api, person, imageID, location).then(function(result){
          console.log(result);
          response.send(result);
      })
      break;


      case 'All':
        // Future
      break;
    }



    //response.send("yeah nah");
  })

})

app.get("/MicrosoftHasImage/:imageID", function(request, response, body){
  var imageID = request.params.imageID;
  con.query("SELECT * FROM microsoftset WHERE imageID = (?)",imageID, function(err, results, fields){
    if(results[0] == typeof(undefined)){
      response.send('0');
    }else{
      response.send('1');
    }

  })
})

// Deterime an image's location on disk
function determineImageLocation(imageID){
  return new Promise(function(fulfill, reject){
      con.query("SELECT * FROM images WHERE ImageID = (?)",imageID, function(err, result, fields){
      var fileName = result[0].fileName;
      //console.log(result[0].fileName);
      var splitName = fileName.split('_');
      var fileEnd = splitName[2].split('.');
      var folder = splitName[0] + "_" + splitName[1];
      if(splitName.length > 3)
      folder+="_"+splitName[2];
      var location = "http://" + address + "/data/images/" + folder+"/"+fileName;
      fulfill(location);
    })
  })
}

function determineMicrosoftHasImage(imageID){
  return new Promise(function(fulfill, reject){
    con.query("SELECT * FROM microsoftset WHERE imageID = (?)",imageID, function(err, results, fields){
      //console.log(results);
      if(results[0] != undefined){
        fulfill(true);
      }else{
        fulfill(false);
      }
    })
  })
}

// Test Face++ API
function testFace(method, api, person, imageID, location){

  // Waiting for a response from the API takes time, need to use promises
  return new Promise(function(fulfill, reject){

    // Determine which kind of test is being undertaken
    switch (method){

      // Basic Facial Recognition
      case "Detection":

        // Need to wait for a response, so use .then() - if we don't any result given will be undefined
        faceAPI.detect().then(function(result){
          fulfill(result);
        })
      break;

      // "Training" - adding image to the api's image set
      case "Training":

        // Not implemented
      break;

      // Comparing two images
      case "Comparison":

        // Not implemented
      break;
    }
  })
}

// Test Microsoft API
function testMicro(method, api, person, imageID, location){

  // Waiting for a response from the API takes time, need to use promises
  return new Promise(function(fulfill, reject){

    // Determine which kind of test is being undertaken
    switch (method){

      // Basic Facial Recognition
      case "Detection":
        microAPI.detect(location).then(function(result){
          fulfill(result);
        })
      break;

      // "Training" - adding image to the api's image set
      // Really gotta rename this bs
      case "Training":
        microAPI.addFace(imageID, location).then(function(result){
          var date = dateTime();
          con.query("INSERT INTO microsoftset (uniqueID, imageID, dateTime) VALUES (?,?,?)",[result.persistedFaceId,imageID,date], function(err){
            if(err)
            throw err;
              console.log("IT APPARENTLY WORKED");
            fulfill("Success");
            reject("Fail");
          });
        })
        // Not implemented
      break;

      case "TrainingStatus":
        microAPI.trainStat().then(function(result){
          fulfill(result);
        })
      break;

        // Comparing two images -Probably scrap this
      case "Comparison":

        // Not implemented
      break;

        // Recognising face
      case "Recognition":

      // Gotta change the way training takes place - probably best to do when  adding images to a set
      //  microAPI.train().then(function(result){

          microAPI.detect(location).then(function(result){
            var faceID = [result[0].faceId];

            console.log(faceID);
            microAPI.recognise(faceID).then(function(result){

              fulfill(result);
            })
          })
        //})

      break;


    }
  })
}

// Test SkyBiometry API
function testSky(method, api, person, imageID, location){

  // Waiting for a response from the API takes time, need to use promises
  return new Promise(function(fulfill, reject){

    // Determine which kind of test is being undertaken
    switch (method){

      // Basic Facial Recognition
      case "Detection":

        skyAPI.detect().then(function(result){
          fulfill(result);
        })
      break;

      // "Training" - adding image to the api's image set
      case "Training":

        // Not implemented
      break;

        // Comparing two images
      case "Comparison":

        // Not implemented
      break;
    }
  })
}

// Test Amazon API
function testAmazon(method, api, person, imageID, location){
  return new Promise(function(resolve, reject){

    switch (method){

      // Basic Facial Recognition
      case "Detection":
        AmazonAPI.detect(location).then(function(result){
          resolve(result);
        })
      break;

      // "Training" - adding image to the api's image set
      case "Training":
      // Facerecords, face, faceID
      AmazonAPI.addFace(location, imageID).then(function(result){
        // grab the uniqueID, add it to the table

        // fullfill/reject accordingly
      })
        // Not implemented
      break;

        // Comparing two images
      case "Recognition":
        AmazonAPI.recognise(location, 95).then(function(results){
          // Store this in the database or Something
          // additionally look at amount of images saved or whatever
          resolve(results);
        })
        // Not implemented
      break;
    }

  })
}

// Start the server on port 80

app.listen(80, function() {
	console.log('Server Started on port 80');

  //Determine the server's ip - Just testing stuff
  publicIp.v4().then(ip => {
    address = ip;
    console.log(ip);
  });
});
