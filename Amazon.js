// Something about needing python to install cl thing
var AWS = require('aws-sdk');
var rekognition = new AWS.Rekognition({region: 'ap-southeast-2'});
var base64Img = require('base64-img');
var fs = require('fs');

var collectionName = 'imageSet';

module.exports = {
  detect: detectFaces,
  addFace: addFace,
  delFace: removeFace,
  listFaces: listFaces,
  recognise: Recognise,
}


function detectFaces(location){

  return new Promise(function(resolve, reject){
    var imageLoc = location;
    var locale = imageLoc.split("/");

    // Not the best way of doing this but I'm tired and want it done now
    var name = locale[locale.length-2];
    var file = locale[locale.length-1];
    var fileLoc = "client/data/images/"+name+"/"+file;
    //console.log(fileLoc);


    //break;
    //var imageLoc = 'client/data/images/Aaron_Eckhart/Aaron_Eckhart_0001.jpg'
    // pull base64 representation of image from file system (or somewhere else)
    fs.readFile(fileLoc, 'base64', (err, data) => {

      // create a new base64 buffer out of the string passed to us by fs.readFile()
      const buffer = new Buffer(data, 'base64');

        var params = {
         Attributes: ["ALL"],
         Image: {
          Bytes: buffer /* Strings will be Base-64 encoded on your behalf */,
         }
        };

        rekognition.detectFaces(params, function(err, data) {
          //if (err) reject(err, err.stack); // an error occurred
          //else{
            //console.log(JSON.stringify(data));

            var face = data.FaceDetails[0];
            var formattedData = {
              age: {
                High: face.AgeRange.High,
                Low: face.AgeRange.Low,
              },
              Beard: {
                Value: face.Beard.Value,
                Confidence: face.Beard.Confidence,
              },
              BoundingBox: {
                Height: face.BoundingBox.Height,
                Left: face.BoundingBox.Left,
                Top: face.BoundingBox.Top,
                Width: face.BoundingBox.Width,
              },
              Confidence: face.Confidence,

              ///
              // Emotions are handled dynamically, new function for that
              ///
              Emotions: face.Emotions,

              Glasses: {
                Value: face.Eyeglasses.Value,
                Confidence: face.Eyeglasses.Confidence,
              },

              EyesOpen: {
                Value: face.EyesOpen.Value,
                Confidence: face.EyesOpen.Confidence,
              },

              Gender: {
                Value: face.Gender.Value,
                Confidence: face.Gender.Confidence,
              },

              Smile: {
                Value: face.Smile.Value,
                Confidence: face.Smile.Confidence,
              },

              MouthOpen: {
                Value: face.MouthOpen.Value,
                Confidence: face.MouthOpen.Confidence,
              },

              Mustache: {
                Value: face.Mustache.Value,
                Confidence: face.Mustache.Confidence,
              },

              Sunglasses: {
                Value: face.Sunglasses.Value,
                Confidence: face.Sunglasses.Confidence,
              },

              Quality: {
                Brightness: face.Quality.Brightness,
                Sharpness: face.Quality.Sharpness,

              },



            };
            //console.log(JSON.stringify(formattedData));

            resolve(formattedData);           // successful response
          //}
          //console.log(data);

        })

  })



  })





};

// Important returned stuff is faceID, imageID
// Probably best to store that
// Use external imageID we give to dert
function addFace(location, imageID){
  return new Promise(function(resolve, reject){

    var imageLoc = location;
    var locale = imageLoc.split("/");

    // Not the best way of doing this but I'm tired and want it done now
    var name = locale[locale.length-2];
    var file = locale[locale.length-1];
    var fileLoc = "client/data/images/"+name+"/"+file;
    //var imageLoc = 'client/data/images/Aaron_Eckhart/Aaron_Eckhart_0001.jpg'
    // pull base64 representation of image from file system (or somewhere else)
    fs.readFile(fileLoc, 'base64', (err, data) => {

        const buffer = new Buffer(data, 'base64');

        var params = {
          CollectionId: collectionName,
          ExternalImageId: imageID,
          Image: {
            Bytes: buffer,
          },
        }

        rekognition.indexFaces(params, function(err, data){


          if (err) reject(err, err.stack); // an error occurred
          else     resolve(data);
        })

    })

  })
}



// Takes an array of faceIDs - those returned by api not our own, and removes them
function removeFace(faceID){
  var params = {
    CollectionId: collectionName,
    FaceIds: faceID
  };

  rekognition.deleteFaces(params, function(err, data){
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);
  })


}

//addFace('test','test','test');
//Recognise(95);

function Recognise(location, threshold = 95){

  return new Promise(function(resolve, reject){

    var imageLoc = location;
    var locale = imageLoc.split("/");

    // Not the best way of doing this but I'm tired and want it done now
    var name = locale[locale.length-2];
    var file = locale[locale.length-1];
    var fileLoc = "client/data/images/"+name+"/"+file;
    //var imageLoc = location;
    //var imageLoc = 'client/data/images/Aaron_Eckhart/Aaron_Eckhart_0001.jpg'
    // pull base64 representation of image from file system (or somewhere else)
    fs.readFile(fileLoc, 'base64', (err, data) => {

        const buffer = new Buffer(data, 'base64');

        var params = {
          CollectionId: collectionName,
          FaceMatchThreshold: threshold,
          Image: {
            Bytes: buffer,

          },
          MaxFaces: 5
        };

        rekognition.searchFacesByImage(params, function(err, data) {
           if (err) reject(err, err.stack); // an error occurred
           else     resolve(data);           // successful response

        })

      })

    })

}


// List facesBox
function listFaces(){

  return new Promise(function(resolve, reject){
    var params = {
      CollectionId: collectionName,
    }

    rekognition.listFaces(params, function(err, data){
      if (err) reject(err, err.stack); // an error occurred
      else     resolve(data);           // successful response
    })
  })

}


// Stuff that needs to be called Once -- ignore for future reference if needed
function createCollection(){
  var params = {
    CollectionId: collectionName
  }

  rekognition.createCollection(params, function(err, data){
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
  })
  // Result
  // { StatusCode: 200,
  //   CollectionArn: 'aws:rekognition:ap-southeast-2:180094471716:collection/imageSet',
  //   FaceModelVersion: '3.0' }


}
