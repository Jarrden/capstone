
var uploadFiles = document.querySelector('#uploadfiles');
var uploadSet = document.querySelector('#uploadSet');
var statusBox = document.getElementById('statusBox');
var peopleStatus = document.getElementById('peopleStatus');
var imageStatus = document.getElementById('imageStatus');
var peopleDropDown = document.getElementById('peopleDrop');
var microsoftAdd = document.getElementById('MicrosoftImagesAdd');
var microsoftDel = document.getElementById('MicrosoftImagesDel');
var microPeople = document.getElementById('microPeople');

microPeople.addEventListener("change", function() {
  updateMicrosoftImages();
});


statusBox.style.display = 'none';

var imagesUploaded = 0;
var imagesFailed = 0;


$("#MicrosoftImagesAdd").imagepicker();
$("#MicrosoftImagesDel").imagepicker();

function filesButton(){
  console.log("wow");
  var files = uploadFiles.files;
  for(var i=0; i<files.length; i++){
     uploadFile(uploadFiles.files[i], peopleDropDown.value); // call the function to upload the file
  }
}

function setButton(){
  var files = uploadSet.files;
  for(var i=0; i<files.length; i++){
     setUpload(uploadSet.files[i]); // call the function to upload the file
  }
}

function uploadFile(file, id){
      statusBox.style.display = 'block';
      imagesUploaded = 0;
      imagesFailed = 0;
	    var url = 'fileupload';
	    var xhr = new XMLHttpRequest();
	    var fd = new FormData();
	    xhr.open("POST", url, true);
	    xhr.onreadystatechange = function() {
	        if (xhr.readyState == 4 && xhr.status == 200) {
	            // Every thing ok, file uploaded
	            //console.log(xhr.responseText); // handle response.
              if(xhr.responseText == 1){
                imagesUploaded++;
              }else{
                imagesFailed++;
              }

              updateStatus();
              console.log("Successful: " + imagesUploaded + ", Failed: " + imagesFailed);


	        }
	    };

      //var test = {test:id};
      fd.append("user", id);
	    fd.append("filetoupload", file);
      console.log(fd);

	    xhr.send(fd);
	}

  function setUpload(file){
    statusBox.style.display = 'block';
    imagesUploaded = 0;
    imagesFailed = 0;
    var url = 'uploadSet';
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Every thing ok, file uploaded
            //console.log(xhr.responseText); // handle response.
            if(xhr.responseText == 1){
              imagesUploaded++;
            }else{
              imagesFailed++;
            }
            updateStatus();
            console.log("Successful: " + imagesUploaded + ", Failed: " + imagesFailed);
        }
    };
    fd.append("filetoupload", file);
    xhr.send(fd);
  }


function updateStatus(){
  peopleStatus.innerHTML = imagesUploaded + "/" + (imagesUploaded+imagesFailed) + " Images Successfully Uploaded.";
  //imageStatus.innerHTML = imagesUploaded + "/" + (imagesUploaded+imagesFailed) + " People Successfully Added.";
}

updatePersonBox();

function updatePersonBox(){
  console.log("Retrieving People");
  $.get("/retrieveNames", function(Result){
    //console.log(Result[0].PersonID);
    var People = "";
    for (var i = 0; i < Result.length; i++) {
      People += '<option value="' + Result[i].PersonID + '">' + Result[i].Name + '</option>';
    }

    peopleDropDown.innerHTML = People;
    microPeople.innerHTML = People;

  });
}

function changeMicrosoftImages(){
  console.log("adding faces" + microPeople.value + microsoftAdd.value);


  $.get("/addFace/Microsoft/" + microPeople.value + "/" + microsoftAdd.value, function(result){
    console.log("It was done apparently");
    setTimeout(updateMicrosoftImages(), 250);
  })

  $.get("/delFace/Microsoft/" + microPeople.value + "/" + microsoftDel.value, function(result){


  console.log("It was done apparently");
  setTimeout(updateMicrosoftImages(), 250);

  })
}

function updateMicrosoftImages(){
  console.log("Retrieving Images");
  var Add = ""
  var Del = ""

  $.get("/retrieveMicroImages/" + microPeople.value +"/test", function(result){

    var results = result;
    //console.log(result);
    //console.log(results.images[0]);

    for (var i = 0; i < results.images.length; i++) {
        console.log(result);
        //Images += '<option data-img-src="' + result.images[i] + '" value="' + result.iDs[i] + '">' + result.names[i] + '</option>'
        if(results.exists[i]){
          //console.log(result.images[i]);
          Del += '<option data-img-src="' + results.images[i] + '" value="' + results.iDs[i] + '">' + results.names[i] + '</option>'
        }else{
          Add += '<option data-img-src="' + results.images[i] + '" value="' + results.iDs[i] + '">' + results.names[i] + '</option>'
        }
        //console.log(result.images[i]);
        microsoftAdd.innerHTML = Add;
        microsoftDel.innerHTML = Del;
        //console.log(Add);
        $("#MicrosoftImagesAdd").imagepicker();
        $("#MicrosoftImagesDel").imagepicker();

        }
  })

}


//console.log("test");
