var peopleDropDown = document.getElementById('people');
var setDropDown = document.getElementById('peopleSet');
var imageDropdown = document.getElementById('images');
var apiDrop = document.getElementById('api');
//var person = document.getElementById('person');
var comparison = document.getElementById('comparison');
var method = document.getElementById('method');
var successBox = document.getElementById('successText');
//var imageBox = document.getElementById('imageBox');
var faceBox = document.getElementById('facesBox');

var faceResults;

var canvas = document.getElementById("myCanvas");
var context = canvas.getContext("2d");
var image = new Image;



setDropDown.addEventListener("change", function() {
  updateImageList();
});



updatePersonBox();

function updatePersonBox(){

  $.get("/retrieveNames", function(Result){

    var People = "";
    People += '<option>--Select Person--</option>'
    for (var i = 0; i < Result.length; i++) {
      People += '<option value="' + Result[i].PersonID + '">' + Result[i].Name + '</option>';
    }

    peopleDropDown.innerHTML = People;
    setDropDown.innerHTML = People;

  });
}

function updateImageList(){
  $.get("/retrieveImages/" + setDropDown.value, function(Result){
    var Images = "";
    Images += '<option>--Select Image--</option>'
    for (var i = 0; i < Result.length; i++) {
      Images += '<option value="' + Result[i].ImageID + '">' + Result[i].fileName + '</option>';
    }
    //console.log(Images);
    imageDropdown.innerHTML = Images;
  })
}

function generateFaceData(result){

  //console.log("Face stuff \n\n"+ result);

  var html = '<ul class="list-group list-group-flush">';
  //var objectNames = Object.getOwnPropertyNames(result);
  //console.log(JSON.stringify(objectNames));


  Object.getOwnPropertyNames(result).forEach(
  function (val, idx, array) {

    html += '<li class="list-group-item">'+val+': '+ (result[val]) + '</li>'

    Object.getOwnPropertyNames(result[val]).forEach(
    function (val1, idx1, array1) {



      var objectConstructor = {}.constructor;
      if(result[val].constructor === objectConstructor){
        if(!Array.isArray(result[val][val1])){
        html += '<li class="list-group-item">'+val +' '+val1 +': '+ JSON.stringify(result[val][val1]) + '</li>'
      }else{
        console.log("not array");
        result[val][val1].forEach(
           function(value, index, arrayThing){

             // This bit is a little hardcoded but whatever
            html += '<li class="list-group-item">HairColour '+value.color +': '+ value.confidence + '</li>'
          }
        );
      }
      }
    }
  );

  }
);
  html += "</ul>";


  return(html);
}

function submit(){

  $.get("/testAPI/"+method.value+"/"+apiDrop.value+"/"+peopleDropDown.value+"/"+imageDropdown.value+"/", function(Result){
    successBox.innerHTML = JSON.stringify(Result);

    for (var i = 0; i < 5; i++) {
      //$("#menu"+(i+1)).hide();
      $("#m"+(i+1)).hide();
    }


    //if(method.value == "Detection"){
      faceResults = Result;
      for (var i = 0; i < Result.length; i++) {
        console.log("weew " + i);
        //$("#menu"+(i+1)).show()
        $("#m"+(i+1)).show();
        $("#menu"+(i+1)).html(generateFaceData(Result[i]));

      }

    //}



    $.get("/retrieveImageLoc/"+imageDropdown.value, function(results){


      var test = "<img src='"+results+"'>";
      console.log(test);
      // Fucken change picture thing here
      setImage(results);


      //imageBox.innerHTML = "<img src='"+results+"'>";
      $("#success").show();
    })

  });


  //successBox.innerHTML = "test";

  // $("#failure").show();
}



// Canvas stuff
function drawBox(x, y, width, height){

  clearImage();
  context.strokeStyle="blue";
  context.strokeRect(x,y,width,height);

  //context.stroke();

}

//image.src = "http://124.187.92.95/data/images/Aaron_Tippin/Aaron_Tippin_0001.jpg";

image.onload = function(){

  context.drawImage(image,0,0);




  console.log("test")

}

function setImage(url){
  image.src = url;
  context.drawImage(image,0,0);



}

function clearImage(){
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.drawImage(image,0,0);

}

function boxFace(faceNum){

  if(faceResults[faceNum] != null && faceResults[faceNum].faceRectangle != null){
    var Result = faceResults[faceNum].faceRectangle;
    var x = Result.left;
    var y = Result.top;
    var width = Result.width;
    var height = Result.height;

    drawBox(x,y,width,height);
  }

}
