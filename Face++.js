
// when detecting from one of our images, given a face_token which expires in 72 hours
// unless added to a Face Set

var rp = require('request-promise');
var request = require('request');
var image = 'https://image.freepik.com/free-photo/family-enjoying-the-summer_1098-1099.jpg';

// Functions to be exported
module.exports.detect = detectFace;


// Facial Recognition
function detectFace(url){

  // Need to use promises in order to be able to wait for a response from the api
  return new Promise(function(fulfill, reject){

    // Call the detect function and format it after receiving an API response
    detect(image).then(function (result){
      var results = "";
      results += "Face++ Detection Complete";
      results += result.faces.length + " Faces Detected";
      results += "\n__________________________";

      for (var i = 0; i < result.faces.length; i++) {

        var attributes = result.faces[i].attributes;

        results += "<br />FACE " + (i+1);
        results += "<br />Gender: " + attributes.gender.value;
        results += "<br />Age: " + attributes.age.value;
        results += "<br />Smile: " + attributes.smile.value;
        results += "<br />Face Quality: Value:" + attributes.facequality.value + " Threshold: " + attributes.facequality.threshold;

        results += "<br />Emotion: \n\t"
                     + "<br />Anger: "      + attributes.emotion.anger + "\n\t"
                     + "<br />Disgust: "    + attributes.emotion.disgust + "\n\t"
                     + "<br />Fear: "       + attributes.emotion.fear + "\n\t"
                     + "<br />Happiness: "  + attributes.emotion.happiness + "\n\t"
                     + "<br />Neutral: "    + attributes.emotion.neutral + "\n\t"
                     + "<br />Sadness: "    + attributes.emotion.sadness + "\n\t"
                     + "<br />Surprise: "   + attributes.emotion.surprise;

        results += "<br /><br />Ethnicity: " + attributes.ethnicity.value;
        results +="<br />__________________________";


        console.log("face token " + result.faces[i].face_token + "\n\n");
      }
      //console.log("face token " + results)
      fulfill(results);

      // If an error occurs, log it to the console
    }).catch(function (err){
      console.log("error reading Results from api \n Error: \t" + err);
    })
  })
}

// Handles connecting to the API and requesting basic facial detection
function detect(imageData){

  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/detect',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR',                            // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc',                         // dont forget to hide this shit
      image_url: imageData,                                                   // Image URL
      return_attributes: 'gender,age,smiling,facequality,emotion,ethnicity'   // Attributes to be returned
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(err.error.error_message);
    })
}

// Compare two faces and decide whether they are from the same person.
function compare(imageData){
  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/compare',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR', // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc', // dont forget to hide this shit
      face_token1: 'd5b0d1185dd6cba968ee2b2a35b6a48b',
      face_token2: 'd5b0d1185dd6cba968ee2b2a35b6a48b', // same image for now
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(error.error_message);
    })
}

// Find one or more most similar faces from Faceset, to a new face.
function search(imageData){
  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/search',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR', // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc', // dont forget to hide this shit
      face_token: 'd5b0d1185dd6cba968ee2b2a35b6a48b',
      faceset_token: '8028ae248e71f8f285e8b606b50b99dc', // will need to test with full set of data
      // return_result_count: 5,
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(error.error_message);
    })
}

//------------------------------ Faces ------------------------------//
// Get face landmarks and attributes by passing its face_token. Can process up to 5 face_token at a time.
function analyze(imageData){
  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/face/analyze',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR', // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc', // dont forget to hide this shit
      face_tokens: 'd5b0d1185dd6cba968ee2b2a35b6a48b',
      // the below option currently gives me un undefined error
      // return_attributes: 'gender, age, smiling, headpose, facequality, blur, eyestatus, emotion, ethnicity, beauty, mouthstatus, eyegaze, skinstatus', // will need to choose which ones we need
      return_landmark: 1,
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(error.error_message);
    })
}

// Get information related to a face by passing its face_token.
function getFaceDetail(imageData){
  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/face/getdetail',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR', // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc', // dont forget to hide this shit
      face_token: 'd5b0d1185dd6cba968ee2b2a35b6a48b',
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(error.error_message);
    })
}

// Set user_id for a detected face. user_id can be returned in Search results to determine the identity of user.
function setUserID(imageData){
  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/face/setuserid',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR', // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc', // dont forget to hide this shit
      face_token: 'd5b0d1185dd6cba968ee2b2a35b6a48b',
      user_id: 'Bond_007', // It is recommended that all the face_token belonging to a same person should be set the same user_id.
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(error.error_message);
    })
}

//----------------------------- FaceSet -----------------------------//
// Handles connecting to the API and requesting basic facial detection
function createFaces(imageData){

  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/faceset/create',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR',                            // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc',                         // dont forget to hide this shit
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(err.error.error_message);
    })
}

// Handles connecting to the API and requesting basic facial detection
function addFace(faceToken){

  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/faceset/addface',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR',                            // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc',                         // dont forget to hide this shit
      faceset_token: '8028ae248e71f8f285e8b606b50b99dc',
      face_tokens: 'd5b0d1185dd6cba968ee2b2a35b6a48b',
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(error.error_message);
    })
}

// Deletes a face from the face set
function deleteFaces(imageData){

  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/faceset/removeface',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR',                            // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc',                         // dont forget to hide this shit
      faceset_token: '8028ae248e71f8f285e8b606b50b99dc',
      face_tokens: 'd5b0d1185dd6cba968ee2b2a35b6a48b',
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(error.message);
    })
}

// Gets information about a face set
function getDetail(imageData){

  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/faceset/getdetail',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR',                            // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc',                         // dont forget to hide this shit
      faceset_token: '8028ae248e71f8f285e8b606b50b99dc',
      start: 1, // An integer between  [1,10000], indicating the sequence number of the face_token in this FaceSet. All the face_token returned in this request will start from this face_token. face_token is sorted by its creation time. Each request returns 100 face_token at the most. You can pass the value of `next `parameter in last request, to get the next 100 face_token. (from: https://console.faceplusplus.com/documents/6329388)
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(err.error.error_message);
    })
}

// Updates the attributes of a FaceSet
// Can update 4 attributes: new_outer_id, display_name, user_data, and tags. Currently only doing the first 2
function update(imageData){
  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/faceset/update',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR',                            // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc',                         // dont forget to hide this shit
      faceset_token: '8028ae248e71f8f285e8b606b50b99dc',
      new_outer_id: 'FaceSetObeject01',
      display_name: 'testDisplayName',
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(error.message);
    })
}

// Get all the FaceSet. Get the list of FaceSet (as well as information such as faceset_token, outer_id, display_name, and tags.)
function getFaceSet(imageData){
  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/faceset/getfacesets',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR',                            // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc',                         // dont forget to hide this shit
      //Otional parameters: tags (String) - Tags of the FaceSet to be searched, comma-seperated, and start (Int)
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(error.message);
    })
}

// Deletes a FaceSet collection
function deleteFaceSet(imageData){
  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://api-us.faceplusplus.com/facepp/v3/faceset/delete',
    qs: {
      api_key: 'QPUxjhNaCxSe8HfogiJsARyXXKY8sRfR',                            // dont forget to hide this shit
      api_secret: '4bmzCax5UrK6CYP9wstHoabFfLnS66yc',                         // dont forget to hide this shit
      faceset_token: '8028ae248e71f8f285e8b606b50b99dc',
      check_empty: 0, // if you set this to 1, it will give you the failed sample response. I'm not sure which one we need just yet.
    },
    json: true                                                                // Return a JSON object
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
        console.log(results);
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(error.message);
    })
}


//TODO list(check it out), train (look into), delete (same as add probs), search(do last)


// Analyse

// Search

// Auxiliary APIs - managing face sets and whatnot

// others : Compare
