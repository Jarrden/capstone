var rp = require('request-promise');
var request = require('request');
var image = 'https://image.freepik.com/free-photo/family-enjoying-the-summer_1098-1099.jpg';

///
/// https://skybiometry.com/documentation/
/// http://jsonviewer.stack.hu/
///


// Functions to be exported
module.exports.detect = detectFace;


// Facial Recognition
function detectFace(url){

  // Need to use promises in order to be able to wait for a response from the api
  return new Promise(function(fulfill, reject){

    // Call the detect function and format it after receiving an API response
    detect(image).then(function (result){
      var results = "";
      results += result.usage.remaining + " Uses Remaining out of " + result.usage.limit;
      results += "Uses reset at: " + result.usage.reset_time_text;

      results += "\n__________________________";


      for (var i = 0; i < result.photos[0].tags.length; i++) {

        var attributes = result.photos[0].tags[i].attributes;

        results += "FACE " + (i+1);
        results += "Is a Face: " + attributes.face.value + ", confidence: " + attributes.face.confidence + "%";
        results += "Gender: " + attributes.gender.value + ", confidence: " + attributes.gender.confidence + "%";
        results += "Smile: " + attributes.smiling.value + ", confidence: " + attributes.smiling.confidence + "%";
        results += "Glasses: " + attributes.glasses.value + ", confidence: " + attributes.glasses.confidence + "%";
        results += "Dark Glasses: " + attributes.dark_glasses.value + ", confidence: " + attributes.dark_glasses.confidence + "%";

        results += "__________________________";

      }

      fulfill(results);

      // If an error occurs, log it to the console
    }).catch(function (err){
      console.log("error reading Results from api \n Error: \t" + err);
    })
  })
}

// Handles connecting to the API and requesting basic facial detection
function detect(imageData){

  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'http://api.skybiometry.com/fc/faces/detect.json',
    qs: {
      api_key: '2t2pti2hlmo63eu5d5qocg4v81',        // dont forget to hide this shit
      api_secret: 'mi5n8vp0ktqhpuem2et1gnasts',
      urls: imageData,
      attributes: 'all'
    },
    json: true
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(err.error.error_message);
    })
}
