var rp = require('request-promise');
var request = require('request');
var image = 'https://image.freepik.com/free-photo/family-enjoying-the-summer_1098-1099.jpg';

var faceListID = 'ifb399testgroup';

// Functions to be exported
//module.exports.detect = detectFace;
module.exports = {
  detect: detect,
  addPerson: addPerson,
  addFace: addFace,
  delFace: delFace,
  listFaces: listFaces,
  recognise: recFace,
  train: trainData,
  trainStat: trainingStatus,
}

// Facial Recognition



/*
  Microsoft's Face List Handling

  In order to be able to do any facial recognition, images will need to be
    added to Microsoft's image list. This should be 100% functional.
  It does not, however, interact with our data - all items are currently hardcoded
    and results aren't being stored yet

  Once faces are added to the list, we need to tell the api to train itself,
  Training shouldn't take long, it can take up to 30mins for 1million images - we won't have that many
  Training Status requests added just to be safe

  Documentation: https://westus.dev.cognitive.microsoft.com/docs/services/563879b61984550e40cbbe8d/operations/5a158c10d2de3616c086f2d3

  Microsoft doesn't like multiple faces in one image. If we ever decided to test with multiple faces, we need to define each face with coordinates
*/




// Handles connecting to the API and requesting basic facial detection
function detect(location){

  // Attributes to POST to the API
  var options = {
    method: 'POST',
    uri: 'https://australiaeast.api.cognitive.microsoft.com/face/v1.0/detect',
    headers: {
      'Ocp-Apim-Subscription-Key': 'f59ed216d28d4dc28740d9b9bac7bd29', // Don't forget to hid that
    },
    body: {
      "url": location,
    },
    qs: {
      returnFaceAttributes: 'age,gender,smile,facialHair,headPose,glasses,emotion,hair,makeup,accessories,blur,exposure,noise'
    },
    json: true
  };


  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
      return (results);
    })
    .catch(function (error){
      console.log("Error detecting Face - problem with API");
      console.log(error);
    })
}


function compare(imageData){

}




// Get the amount of faces in the list, can return an array of ids and values
function listFaces(){

  var options = {
    method: 'GET',
    uri: 'https://australiaeast.api.cognitive.microsoft.com/face/v1.0/persongroups/' + faceListID +'/persons?start=&top=',
    headers: {
      'Ocp-Apim-Subscription-Key': 'f59ed216d28d4dc28740d9b9bac7bd29', // Don't forget to hid that
    },
    json: true
  };


  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
      console.log("Face List Count Call Successful");
      console.log(results.length + " Faces Listed");
      for (var i = 0; i < results.length; i++) {
        console.log("ID: " + results[i].persistedFaceId + "\t\tData: " + results[i].name);
      }


    })
    .catch(function (error){
      console.log("Face list count call failed");
      console.log(error.message);
    })
}

// Request Training Status from API
function trainingStatus(){

  var options = {
    method: 'GET',
    uri: 'https://australiaeast.api.cognitive.microsoft.com/face/v1.0/persongroups/' + faceListID +'/training',
    headers: {
      'Ocp-Apim-Subscription-Key': 'f59ed216d28d4dc28740d9b9bac7bd29', // Don't forget to hid that
    },
    json: true
  };


  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){

      var trainStatus = {
        Status: results.status,
        createdDateTime: results.createdDateTime,
        lastActionDateTime: results.lastActionDateTime,
        lastSuccessfulTrainingDateTime: results.lastSuccessfulTrainingDateTime,
        //Message: results.message,
      }
      var finalResult = [];
      finalResult[0] = trainStatus;

      console.log("Training Status Call Successful");
      console.log("Status: " + results.status);
      console.log("createdDateTime: " + results.createdDateTime);
      console.log("lastActionDateTime: " + results.lastActionDateTime);
      console.log("lastSuccessfulTrainingDateTime: " + results.lastSuccessfulTrainingDateTime);
      console.log("Message: " + results.message);

      return finalResult;
    })
    .catch(function (error){
      console.log("Training Status Call Failed");
      console.log(error.message);
    })
}

// Tell api to train itself
function trainData(){

  var options = {
    method: 'POST',
    uri: 'https://australiaeast.api.cognitive.microsoft.com/face/v1.0/persongroups/' + faceListID +'/train',
    headers: {
      'Ocp-Apim-Subscription-Key': 'f59ed216d28d4dc28740d9b9bac7bd29', // Don't forget to hid that
    },
    json: true
  };

  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
      console.log("Training Call Successful");
    })
    .catch(function (error){
      console.log("Training Call Failed");
      console.log(error.message);
    })

}

// Remove a face from the image list
function delFace(uniqueID, faceID){

  var options = {
    method: 'DELETE',
    uri: 'https://australiaeast.api.cognitive.microsoft.com/face/v1.0/persongroups/ifb399testgroup/persons/'+uniqueID+'/persistedfaces/' + faceID,
    headers: {
      'Ocp-Apim-Subscription-Key': 'f59ed216d28d4dc28740d9b9bac7bd29', // Don't forget to hid that
    },
    json: true
  };


  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
      console.log("Image Deleted Successfully");
    })
    .catch(function (error){
      console.log("Image Deletion Failed");
      console.log(error.message);
    })

}

// Add a face to Microsoft's image list
function addFace(uniqueID, location){
  var imageName = "test";
  console.log("IMAGE URL: " + location);
  var options = {
    method: 'POST',
    uri: 'https://australiaeast.api.cognitive.microsoft.com/face/v1.0/persongroups/ifb399testgroup/persons/' + uniqueID + '/persistedFaces',
    headers: {
      'Ocp-Apim-Subscription-Key': 'f59ed216d28d4dc28740d9b9bac7bd29', // Don't forget to hid that
    },
    body: {
      "url": location,
    },
    json: true
  };


  // Use Request Promise Module to handle api call, and log results/errors
  return rp(options)
    .then(function (results){
      console.log("Image Added Successfully");
      console.log("Persistant ID for image: " + results.persistedFaceId);
      return (results.persistedFaceId);
    })
    .catch(function (error){
      console.log("Error adding Image - problem with API");
      console.log(error.message);
    })
}

// Add a person to image list
function addPerson(name){

    var options = {
      method: 'POST',
      uri: 'https://australiaeast.api.cognitive.microsoft.com/face/v1.0/persongroups/ifb399testgroup/persons',
      headers: {
        'Ocp-Apim-Subscription-Key': 'f59ed216d28d4dc28740d9b9bac7bd29', // Don't forget to hid that
      },
      body: {
        "name": name,
      },
      json: true
    };

    return rp(options).then(function (results){
      console.log("Person Added Successfully, ID: " + results.personId);
      return results.personId;

    }).catch(function (error){
      console.log("Person Not Added");
      console.log(error.error);
    })

}

// Identify a person from a person list using an image
function recFace(faceIds){
  var options = {
    method: 'POST',
    uri: 'https://australiaeast.api.cognitive.microsoft.com/face/v1.0/identify',
    headers: {
      'Ocp-Apim-Subscription-Key': 'f59ed216d28d4dc28740d9b9bac7bd29', // Don't forget to hid that
    },
    body: {
      faceIds: faceIds,
      personGroupId: faceListID,
    },
    json: true
  };

  return rp(options).then(function (results){

    var finalResults = [];

    for (var i = 0; i < results[0].candidates.length; i++) {
      var indResult = {
        personID: results[0].candidates[i].personId,
        confidence: results[0].candidates[i].confidence,
      };
      finalResults[0] = indResult;
    }

    //console.log("Person Added Successfully, ID: " + results);
    return finalResults;

  }).catch(function (error){
    console.log("Person Not Added");
    console.log(error.error);
  })
}
